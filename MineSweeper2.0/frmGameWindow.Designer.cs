﻿namespace MineSweeper2._0
{
    partial class frmGameWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmGameWindow));
            this.lblTitle = new System.Windows.Forms.Label();
            this.pnlBoard = new System.Windows.Forms.Panel();
            this.btn88 = new System.Windows.Forms.Button();
            this.btn87 = new System.Windows.Forms.Button();
            this.btn86 = new System.Windows.Forms.Button();
            this.btn85 = new System.Windows.Forms.Button();
            this.btn84 = new System.Windows.Forms.Button();
            this.btn83 = new System.Windows.Forms.Button();
            this.btn82 = new System.Windows.Forms.Button();
            this.btn81 = new System.Windows.Forms.Button();
            this.btn80 = new System.Windows.Forms.Button();
            this.btn78 = new System.Windows.Forms.Button();
            this.btn77 = new System.Windows.Forms.Button();
            this.btn76 = new System.Windows.Forms.Button();
            this.btn75 = new System.Windows.Forms.Button();
            this.btn74 = new System.Windows.Forms.Button();
            this.btn73 = new System.Windows.Forms.Button();
            this.btn72 = new System.Windows.Forms.Button();
            this.btn71 = new System.Windows.Forms.Button();
            this.btn70 = new System.Windows.Forms.Button();
            this.btn68 = new System.Windows.Forms.Button();
            this.btn67 = new System.Windows.Forms.Button();
            this.btn66 = new System.Windows.Forms.Button();
            this.btn65 = new System.Windows.Forms.Button();
            this.btn64 = new System.Windows.Forms.Button();
            this.btn63 = new System.Windows.Forms.Button();
            this.btn62 = new System.Windows.Forms.Button();
            this.btn61 = new System.Windows.Forms.Button();
            this.btn60 = new System.Windows.Forms.Button();
            this.btn58 = new System.Windows.Forms.Button();
            this.btn57 = new System.Windows.Forms.Button();
            this.btn56 = new System.Windows.Forms.Button();
            this.btn55 = new System.Windows.Forms.Button();
            this.btn54 = new System.Windows.Forms.Button();
            this.btn53 = new System.Windows.Forms.Button();
            this.btn52 = new System.Windows.Forms.Button();
            this.btn51 = new System.Windows.Forms.Button();
            this.btn50 = new System.Windows.Forms.Button();
            this.btn48 = new System.Windows.Forms.Button();
            this.btn47 = new System.Windows.Forms.Button();
            this.btn46 = new System.Windows.Forms.Button();
            this.btn45 = new System.Windows.Forms.Button();
            this.btn44 = new System.Windows.Forms.Button();
            this.btn43 = new System.Windows.Forms.Button();
            this.btn42 = new System.Windows.Forms.Button();
            this.btn41 = new System.Windows.Forms.Button();
            this.btn40 = new System.Windows.Forms.Button();
            this.btn38 = new System.Windows.Forms.Button();
            this.btn37 = new System.Windows.Forms.Button();
            this.btn36 = new System.Windows.Forms.Button();
            this.btn35 = new System.Windows.Forms.Button();
            this.btn34 = new System.Windows.Forms.Button();
            this.btn33 = new System.Windows.Forms.Button();
            this.btn32 = new System.Windows.Forms.Button();
            this.btn31 = new System.Windows.Forms.Button();
            this.btn30 = new System.Windows.Forms.Button();
            this.btn28 = new System.Windows.Forms.Button();
            this.btn27 = new System.Windows.Forms.Button();
            this.btn26 = new System.Windows.Forms.Button();
            this.btn25 = new System.Windows.Forms.Button();
            this.btn24 = new System.Windows.Forms.Button();
            this.btn23 = new System.Windows.Forms.Button();
            this.btn22 = new System.Windows.Forms.Button();
            this.btn21 = new System.Windows.Forms.Button();
            this.btn20 = new System.Windows.Forms.Button();
            this.btn18 = new System.Windows.Forms.Button();
            this.btn17 = new System.Windows.Forms.Button();
            this.btn16 = new System.Windows.Forms.Button();
            this.btn15 = new System.Windows.Forms.Button();
            this.btn14 = new System.Windows.Forms.Button();
            this.btn13 = new System.Windows.Forms.Button();
            this.btn12 = new System.Windows.Forms.Button();
            this.btn11 = new System.Windows.Forms.Button();
            this.btn10 = new System.Windows.Forms.Button();
            this.btn08 = new System.Windows.Forms.Button();
            this.btn07 = new System.Windows.Forms.Button();
            this.btn06 = new System.Windows.Forms.Button();
            this.btn05 = new System.Windows.Forms.Button();
            this.btn04 = new System.Windows.Forms.Button();
            this.btn03 = new System.Windows.Forms.Button();
            this.btn02 = new System.Windows.Forms.Button();
            this.btn01 = new System.Windows.Forms.Button();
            this.btn00 = new System.Windows.Forms.Button();
            this.pnlBoard.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.Font = new System.Drawing.Font("Lucida Handwriting", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitle.Location = new System.Drawing.Point(13, 9);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(282, 23);
            this.lblTitle.TabIndex = 1;
            this.lblTitle.Text = "MineSweeper FTW!";
            this.lblTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pnlBoard
            // 
            this.pnlBoard.Controls.Add(this.btn88);
            this.pnlBoard.Controls.Add(this.btn87);
            this.pnlBoard.Controls.Add(this.btn86);
            this.pnlBoard.Controls.Add(this.btn85);
            this.pnlBoard.Controls.Add(this.btn84);
            this.pnlBoard.Controls.Add(this.btn83);
            this.pnlBoard.Controls.Add(this.btn82);
            this.pnlBoard.Controls.Add(this.btn81);
            this.pnlBoard.Controls.Add(this.btn80);
            this.pnlBoard.Controls.Add(this.btn78);
            this.pnlBoard.Controls.Add(this.btn77);
            this.pnlBoard.Controls.Add(this.btn76);
            this.pnlBoard.Controls.Add(this.btn75);
            this.pnlBoard.Controls.Add(this.btn74);
            this.pnlBoard.Controls.Add(this.btn73);
            this.pnlBoard.Controls.Add(this.btn72);
            this.pnlBoard.Controls.Add(this.btn71);
            this.pnlBoard.Controls.Add(this.btn70);
            this.pnlBoard.Controls.Add(this.btn68);
            this.pnlBoard.Controls.Add(this.btn67);
            this.pnlBoard.Controls.Add(this.btn66);
            this.pnlBoard.Controls.Add(this.btn65);
            this.pnlBoard.Controls.Add(this.btn64);
            this.pnlBoard.Controls.Add(this.btn63);
            this.pnlBoard.Controls.Add(this.btn62);
            this.pnlBoard.Controls.Add(this.btn61);
            this.pnlBoard.Controls.Add(this.btn60);
            this.pnlBoard.Controls.Add(this.btn58);
            this.pnlBoard.Controls.Add(this.btn57);
            this.pnlBoard.Controls.Add(this.btn56);
            this.pnlBoard.Controls.Add(this.btn55);
            this.pnlBoard.Controls.Add(this.btn54);
            this.pnlBoard.Controls.Add(this.btn53);
            this.pnlBoard.Controls.Add(this.btn52);
            this.pnlBoard.Controls.Add(this.btn51);
            this.pnlBoard.Controls.Add(this.btn50);
            this.pnlBoard.Controls.Add(this.btn48);
            this.pnlBoard.Controls.Add(this.btn47);
            this.pnlBoard.Controls.Add(this.btn46);
            this.pnlBoard.Controls.Add(this.btn45);
            this.pnlBoard.Controls.Add(this.btn44);
            this.pnlBoard.Controls.Add(this.btn43);
            this.pnlBoard.Controls.Add(this.btn42);
            this.pnlBoard.Controls.Add(this.btn41);
            this.pnlBoard.Controls.Add(this.btn40);
            this.pnlBoard.Controls.Add(this.btn38);
            this.pnlBoard.Controls.Add(this.btn37);
            this.pnlBoard.Controls.Add(this.btn36);
            this.pnlBoard.Controls.Add(this.btn35);
            this.pnlBoard.Controls.Add(this.btn34);
            this.pnlBoard.Controls.Add(this.btn33);
            this.pnlBoard.Controls.Add(this.btn32);
            this.pnlBoard.Controls.Add(this.btn31);
            this.pnlBoard.Controls.Add(this.btn30);
            this.pnlBoard.Controls.Add(this.btn28);
            this.pnlBoard.Controls.Add(this.btn27);
            this.pnlBoard.Controls.Add(this.btn26);
            this.pnlBoard.Controls.Add(this.btn25);
            this.pnlBoard.Controls.Add(this.btn24);
            this.pnlBoard.Controls.Add(this.btn23);
            this.pnlBoard.Controls.Add(this.btn22);
            this.pnlBoard.Controls.Add(this.btn21);
            this.pnlBoard.Controls.Add(this.btn20);
            this.pnlBoard.Controls.Add(this.btn18);
            this.pnlBoard.Controls.Add(this.btn17);
            this.pnlBoard.Controls.Add(this.btn16);
            this.pnlBoard.Controls.Add(this.btn15);
            this.pnlBoard.Controls.Add(this.btn14);
            this.pnlBoard.Controls.Add(this.btn13);
            this.pnlBoard.Controls.Add(this.btn12);
            this.pnlBoard.Controls.Add(this.btn11);
            this.pnlBoard.Controls.Add(this.btn10);
            this.pnlBoard.Controls.Add(this.btn08);
            this.pnlBoard.Controls.Add(this.btn07);
            this.pnlBoard.Controls.Add(this.btn06);
            this.pnlBoard.Controls.Add(this.btn05);
            this.pnlBoard.Controls.Add(this.btn04);
            this.pnlBoard.Controls.Add(this.btn03);
            this.pnlBoard.Controls.Add(this.btn02);
            this.pnlBoard.Controls.Add(this.btn01);
            this.pnlBoard.Controls.Add(this.btn00);
            this.pnlBoard.Location = new System.Drawing.Point(12, 41);
            this.pnlBoard.Name = "pnlBoard";
            this.pnlBoard.Size = new System.Drawing.Size(284, 284);
            this.pnlBoard.TabIndex = 2;
            // 
            // btn88
            // 
            this.btn88.Location = new System.Drawing.Point(250, 250);
            this.btn88.Name = "btn88";
            this.btn88.Size = new System.Drawing.Size(32, 32);
            this.btn88.TabIndex = 80;
            this.btn88.UseVisualStyleBackColor = true;
            this.btn88.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ButtonClickCheck);
            // 
            // btn87
            // 
            this.btn87.Location = new System.Drawing.Point(219, 250);
            this.btn87.Name = "btn87";
            this.btn87.Size = new System.Drawing.Size(32, 32);
            this.btn87.TabIndex = 79;
            this.btn87.UseVisualStyleBackColor = true;
            this.btn87.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ButtonClickCheck);
            // 
            // btn86
            // 
            this.btn86.Location = new System.Drawing.Point(188, 250);
            this.btn86.Name = "btn86";
            this.btn86.Size = new System.Drawing.Size(32, 32);
            this.btn86.TabIndex = 78;
            this.btn86.UseVisualStyleBackColor = true;
            this.btn86.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ButtonClickCheck);
            // 
            // btn85
            // 
            this.btn85.Location = new System.Drawing.Point(157, 250);
            this.btn85.Name = "btn85";
            this.btn85.Size = new System.Drawing.Size(32, 32);
            this.btn85.TabIndex = 77;
            this.btn85.UseVisualStyleBackColor = true;
            this.btn85.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ButtonClickCheck);
            // 
            // btn84
            // 
            this.btn84.Location = new System.Drawing.Point(126, 250);
            this.btn84.Name = "btn84";
            this.btn84.Size = new System.Drawing.Size(32, 32);
            this.btn84.TabIndex = 76;
            this.btn84.UseVisualStyleBackColor = true;
            this.btn84.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ButtonClickCheck);
            // 
            // btn83
            // 
            this.btn83.Location = new System.Drawing.Point(95, 250);
            this.btn83.Name = "btn83";
            this.btn83.Size = new System.Drawing.Size(32, 32);
            this.btn83.TabIndex = 75;
            this.btn83.UseVisualStyleBackColor = true;
            this.btn83.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ButtonClickCheck);
            // 
            // btn82
            // 
            this.btn82.Location = new System.Drawing.Point(64, 250);
            this.btn82.Name = "btn82";
            this.btn82.Size = new System.Drawing.Size(32, 32);
            this.btn82.TabIndex = 74;
            this.btn82.UseVisualStyleBackColor = true;
            this.btn82.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ButtonClickCheck);
            // 
            // btn81
            // 
            this.btn81.Location = new System.Drawing.Point(33, 250);
            this.btn81.Name = "btn81";
            this.btn81.Size = new System.Drawing.Size(32, 32);
            this.btn81.TabIndex = 73;
            this.btn81.UseVisualStyleBackColor = true;
            this.btn81.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ButtonClickCheck);
            // 
            // btn80
            // 
            this.btn80.Location = new System.Drawing.Point(2, 250);
            this.btn80.Name = "btn80";
            this.btn80.Size = new System.Drawing.Size(32, 32);
            this.btn80.TabIndex = 72;
            this.btn80.UseVisualStyleBackColor = true;
            this.btn80.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ButtonClickCheck);
            // 
            // btn78
            // 
            this.btn78.Location = new System.Drawing.Point(250, 219);
            this.btn78.Name = "btn78";
            this.btn78.Size = new System.Drawing.Size(32, 32);
            this.btn78.TabIndex = 71;
            this.btn78.UseVisualStyleBackColor = true;
            this.btn78.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ButtonClickCheck);
            // 
            // btn77
            // 
            this.btn77.Location = new System.Drawing.Point(219, 219);
            this.btn77.Name = "btn77";
            this.btn77.Size = new System.Drawing.Size(32, 32);
            this.btn77.TabIndex = 70;
            this.btn77.UseVisualStyleBackColor = true;
            this.btn77.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ButtonClickCheck);
            // 
            // btn76
            // 
            this.btn76.Location = new System.Drawing.Point(188, 219);
            this.btn76.Name = "btn76";
            this.btn76.Size = new System.Drawing.Size(32, 32);
            this.btn76.TabIndex = 69;
            this.btn76.UseVisualStyleBackColor = true;
            this.btn76.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ButtonClickCheck);
            // 
            // btn75
            // 
            this.btn75.Location = new System.Drawing.Point(157, 219);
            this.btn75.Name = "btn75";
            this.btn75.Size = new System.Drawing.Size(32, 32);
            this.btn75.TabIndex = 68;
            this.btn75.UseVisualStyleBackColor = true;
            this.btn75.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ButtonClickCheck);
            // 
            // btn74
            // 
            this.btn74.Location = new System.Drawing.Point(126, 219);
            this.btn74.Name = "btn74";
            this.btn74.Size = new System.Drawing.Size(32, 32);
            this.btn74.TabIndex = 67;
            this.btn74.UseVisualStyleBackColor = true;
            this.btn74.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ButtonClickCheck);
            // 
            // btn73
            // 
            this.btn73.Location = new System.Drawing.Point(95, 219);
            this.btn73.Name = "btn73";
            this.btn73.Size = new System.Drawing.Size(32, 32);
            this.btn73.TabIndex = 66;
            this.btn73.UseVisualStyleBackColor = true;
            this.btn73.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ButtonClickCheck);
            // 
            // btn72
            // 
            this.btn72.Location = new System.Drawing.Point(64, 219);
            this.btn72.Name = "btn72";
            this.btn72.Size = new System.Drawing.Size(32, 32);
            this.btn72.TabIndex = 65;
            this.btn72.UseVisualStyleBackColor = true;
            this.btn72.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ButtonClickCheck);
            // 
            // btn71
            // 
            this.btn71.Location = new System.Drawing.Point(33, 219);
            this.btn71.Name = "btn71";
            this.btn71.Size = new System.Drawing.Size(32, 32);
            this.btn71.TabIndex = 64;
            this.btn71.UseVisualStyleBackColor = true;
            this.btn71.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ButtonClickCheck);
            // 
            // btn70
            // 
            this.btn70.Location = new System.Drawing.Point(2, 219);
            this.btn70.Name = "btn70";
            this.btn70.Size = new System.Drawing.Size(32, 32);
            this.btn70.TabIndex = 63;
            this.btn70.UseVisualStyleBackColor = true;
            this.btn70.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ButtonClickCheck);
            // 
            // btn68
            // 
            this.btn68.Location = new System.Drawing.Point(250, 188);
            this.btn68.Name = "btn68";
            this.btn68.Size = new System.Drawing.Size(32, 32);
            this.btn68.TabIndex = 62;
            this.btn68.UseVisualStyleBackColor = true;
            this.btn68.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ButtonClickCheck);
            // 
            // btn67
            // 
            this.btn67.Location = new System.Drawing.Point(219, 188);
            this.btn67.Name = "btn67";
            this.btn67.Size = new System.Drawing.Size(32, 32);
            this.btn67.TabIndex = 61;
            this.btn67.UseVisualStyleBackColor = true;
            this.btn67.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ButtonClickCheck);
            // 
            // btn66
            // 
            this.btn66.Location = new System.Drawing.Point(188, 188);
            this.btn66.Name = "btn66";
            this.btn66.Size = new System.Drawing.Size(32, 32);
            this.btn66.TabIndex = 60;
            this.btn66.UseVisualStyleBackColor = true;
            this.btn66.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ButtonClickCheck);
            // 
            // btn65
            // 
            this.btn65.Location = new System.Drawing.Point(157, 188);
            this.btn65.Name = "btn65";
            this.btn65.Size = new System.Drawing.Size(32, 32);
            this.btn65.TabIndex = 59;
            this.btn65.UseVisualStyleBackColor = true;
            this.btn65.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ButtonClickCheck);
            // 
            // btn64
            // 
            this.btn64.Location = new System.Drawing.Point(126, 188);
            this.btn64.Name = "btn64";
            this.btn64.Size = new System.Drawing.Size(32, 32);
            this.btn64.TabIndex = 58;
            this.btn64.UseVisualStyleBackColor = true;
            this.btn64.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ButtonClickCheck);
            // 
            // btn63
            // 
            this.btn63.Location = new System.Drawing.Point(95, 188);
            this.btn63.Name = "btn63";
            this.btn63.Size = new System.Drawing.Size(32, 32);
            this.btn63.TabIndex = 57;
            this.btn63.UseVisualStyleBackColor = true;
            this.btn63.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ButtonClickCheck);
            // 
            // btn62
            // 
            this.btn62.Location = new System.Drawing.Point(64, 188);
            this.btn62.Name = "btn62";
            this.btn62.Size = new System.Drawing.Size(32, 32);
            this.btn62.TabIndex = 56;
            this.btn62.UseVisualStyleBackColor = true;
            this.btn62.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ButtonClickCheck);
            // 
            // btn61
            // 
            this.btn61.Location = new System.Drawing.Point(33, 188);
            this.btn61.Name = "btn61";
            this.btn61.Size = new System.Drawing.Size(32, 32);
            this.btn61.TabIndex = 55;
            this.btn61.UseVisualStyleBackColor = true;
            this.btn61.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ButtonClickCheck);
            // 
            // btn60
            // 
            this.btn60.Location = new System.Drawing.Point(2, 188);
            this.btn60.Name = "btn60";
            this.btn60.Size = new System.Drawing.Size(32, 32);
            this.btn60.TabIndex = 54;
            this.btn60.UseVisualStyleBackColor = true;
            this.btn60.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ButtonClickCheck);
            // 
            // btn58
            // 
            this.btn58.Location = new System.Drawing.Point(250, 157);
            this.btn58.Name = "btn58";
            this.btn58.Size = new System.Drawing.Size(32, 32);
            this.btn58.TabIndex = 53;
            this.btn58.UseVisualStyleBackColor = true;
            this.btn58.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ButtonClickCheck);
            // 
            // btn57
            // 
            this.btn57.Location = new System.Drawing.Point(219, 157);
            this.btn57.Name = "btn57";
            this.btn57.Size = new System.Drawing.Size(32, 32);
            this.btn57.TabIndex = 52;
            this.btn57.UseVisualStyleBackColor = true;
            this.btn57.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ButtonClickCheck);
            // 
            // btn56
            // 
            this.btn56.Location = new System.Drawing.Point(188, 157);
            this.btn56.Name = "btn56";
            this.btn56.Size = new System.Drawing.Size(32, 32);
            this.btn56.TabIndex = 51;
            this.btn56.UseVisualStyleBackColor = true;
            this.btn56.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ButtonClickCheck);
            // 
            // btn55
            // 
            this.btn55.Location = new System.Drawing.Point(157, 157);
            this.btn55.Name = "btn55";
            this.btn55.Size = new System.Drawing.Size(32, 32);
            this.btn55.TabIndex = 50;
            this.btn55.UseVisualStyleBackColor = true;
            this.btn55.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ButtonClickCheck);
            // 
            // btn54
            // 
            this.btn54.Location = new System.Drawing.Point(126, 157);
            this.btn54.Name = "btn54";
            this.btn54.Size = new System.Drawing.Size(32, 32);
            this.btn54.TabIndex = 49;
            this.btn54.UseVisualStyleBackColor = true;
            this.btn54.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ButtonClickCheck);
            // 
            // btn53
            // 
            this.btn53.Location = new System.Drawing.Point(95, 157);
            this.btn53.Name = "btn53";
            this.btn53.Size = new System.Drawing.Size(32, 32);
            this.btn53.TabIndex = 48;
            this.btn53.UseVisualStyleBackColor = true;
            this.btn53.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ButtonClickCheck);
            // 
            // btn52
            // 
            this.btn52.Location = new System.Drawing.Point(64, 157);
            this.btn52.Name = "btn52";
            this.btn52.Size = new System.Drawing.Size(32, 32);
            this.btn52.TabIndex = 47;
            this.btn52.UseVisualStyleBackColor = true;
            this.btn52.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ButtonClickCheck);
            // 
            // btn51
            // 
            this.btn51.Location = new System.Drawing.Point(33, 157);
            this.btn51.Name = "btn51";
            this.btn51.Size = new System.Drawing.Size(32, 32);
            this.btn51.TabIndex = 46;
            this.btn51.UseVisualStyleBackColor = true;
            this.btn51.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ButtonClickCheck);
            // 
            // btn50
            // 
            this.btn50.Location = new System.Drawing.Point(2, 157);
            this.btn50.Name = "btn50";
            this.btn50.Size = new System.Drawing.Size(32, 32);
            this.btn50.TabIndex = 45;
            this.btn50.UseVisualStyleBackColor = true;
            this.btn50.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ButtonClickCheck);
            // 
            // btn48
            // 
            this.btn48.Location = new System.Drawing.Point(250, 126);
            this.btn48.Name = "btn48";
            this.btn48.Size = new System.Drawing.Size(32, 32);
            this.btn48.TabIndex = 44;
            this.btn48.UseVisualStyleBackColor = true;
            this.btn48.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ButtonClickCheck);
            // 
            // btn47
            // 
            this.btn47.Location = new System.Drawing.Point(219, 126);
            this.btn47.Name = "btn47";
            this.btn47.Size = new System.Drawing.Size(32, 32);
            this.btn47.TabIndex = 43;
            this.btn47.UseVisualStyleBackColor = true;
            this.btn47.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ButtonClickCheck);
            // 
            // btn46
            // 
            this.btn46.Location = new System.Drawing.Point(188, 126);
            this.btn46.Name = "btn46";
            this.btn46.Size = new System.Drawing.Size(32, 32);
            this.btn46.TabIndex = 42;
            this.btn46.UseVisualStyleBackColor = true;
            this.btn46.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ButtonClickCheck);
            // 
            // btn45
            // 
            this.btn45.Location = new System.Drawing.Point(157, 126);
            this.btn45.Name = "btn45";
            this.btn45.Size = new System.Drawing.Size(32, 32);
            this.btn45.TabIndex = 41;
            this.btn45.UseVisualStyleBackColor = true;
            this.btn45.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ButtonClickCheck);
            // 
            // btn44
            // 
            this.btn44.Location = new System.Drawing.Point(126, 126);
            this.btn44.Name = "btn44";
            this.btn44.Size = new System.Drawing.Size(32, 32);
            this.btn44.TabIndex = 40;
            this.btn44.UseVisualStyleBackColor = true;
            this.btn44.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ButtonClickCheck);
            // 
            // btn43
            // 
            this.btn43.Location = new System.Drawing.Point(95, 126);
            this.btn43.Name = "btn43";
            this.btn43.Size = new System.Drawing.Size(32, 32);
            this.btn43.TabIndex = 39;
            this.btn43.UseVisualStyleBackColor = true;
            this.btn43.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ButtonClickCheck);
            // 
            // btn42
            // 
            this.btn42.Location = new System.Drawing.Point(64, 126);
            this.btn42.Name = "btn42";
            this.btn42.Size = new System.Drawing.Size(32, 32);
            this.btn42.TabIndex = 38;
            this.btn42.UseVisualStyleBackColor = true;
            this.btn42.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ButtonClickCheck);
            // 
            // btn41
            // 
            this.btn41.Location = new System.Drawing.Point(33, 126);
            this.btn41.Name = "btn41";
            this.btn41.Size = new System.Drawing.Size(32, 32);
            this.btn41.TabIndex = 37;
            this.btn41.UseVisualStyleBackColor = true;
            this.btn41.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ButtonClickCheck);
            // 
            // btn40
            // 
            this.btn40.Location = new System.Drawing.Point(2, 126);
            this.btn40.Name = "btn40";
            this.btn40.Size = new System.Drawing.Size(32, 32);
            this.btn40.TabIndex = 36;
            this.btn40.UseVisualStyleBackColor = true;
            this.btn40.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ButtonClickCheck);
            // 
            // btn38
            // 
            this.btn38.Location = new System.Drawing.Point(250, 95);
            this.btn38.Name = "btn38";
            this.btn38.Size = new System.Drawing.Size(32, 32);
            this.btn38.TabIndex = 35;
            this.btn38.UseVisualStyleBackColor = true;
            this.btn38.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ButtonClickCheck);
            // 
            // btn37
            // 
            this.btn37.Location = new System.Drawing.Point(219, 95);
            this.btn37.Name = "btn37";
            this.btn37.Size = new System.Drawing.Size(32, 32);
            this.btn37.TabIndex = 34;
            this.btn37.UseVisualStyleBackColor = true;
            this.btn37.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ButtonClickCheck);
            // 
            // btn36
            // 
            this.btn36.Location = new System.Drawing.Point(188, 95);
            this.btn36.Name = "btn36";
            this.btn36.Size = new System.Drawing.Size(32, 32);
            this.btn36.TabIndex = 33;
            this.btn36.UseVisualStyleBackColor = true;
            this.btn36.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ButtonClickCheck);
            // 
            // btn35
            // 
            this.btn35.Location = new System.Drawing.Point(157, 95);
            this.btn35.Name = "btn35";
            this.btn35.Size = new System.Drawing.Size(32, 32);
            this.btn35.TabIndex = 32;
            this.btn35.UseVisualStyleBackColor = true;
            this.btn35.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ButtonClickCheck);
            // 
            // btn34
            // 
            this.btn34.Location = new System.Drawing.Point(126, 95);
            this.btn34.Name = "btn34";
            this.btn34.Size = new System.Drawing.Size(32, 32);
            this.btn34.TabIndex = 31;
            this.btn34.UseVisualStyleBackColor = true;
            this.btn34.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ButtonClickCheck);
            // 
            // btn33
            // 
            this.btn33.Location = new System.Drawing.Point(95, 95);
            this.btn33.Name = "btn33";
            this.btn33.Size = new System.Drawing.Size(32, 32);
            this.btn33.TabIndex = 30;
            this.btn33.UseVisualStyleBackColor = true;
            this.btn33.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ButtonClickCheck);
            // 
            // btn32
            // 
            this.btn32.Location = new System.Drawing.Point(64, 95);
            this.btn32.Name = "btn32";
            this.btn32.Size = new System.Drawing.Size(32, 32);
            this.btn32.TabIndex = 29;
            this.btn32.UseVisualStyleBackColor = true;
            this.btn32.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ButtonClickCheck);
            // 
            // btn31
            // 
            this.btn31.Location = new System.Drawing.Point(33, 95);
            this.btn31.Name = "btn31";
            this.btn31.Size = new System.Drawing.Size(32, 32);
            this.btn31.TabIndex = 28;
            this.btn31.UseVisualStyleBackColor = true;
            this.btn31.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ButtonClickCheck);
            // 
            // btn30
            // 
            this.btn30.Location = new System.Drawing.Point(2, 95);
            this.btn30.Name = "btn30";
            this.btn30.Size = new System.Drawing.Size(32, 32);
            this.btn30.TabIndex = 27;
            this.btn30.UseVisualStyleBackColor = true;
            this.btn30.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ButtonClickCheck);
            // 
            // btn28
            // 
            this.btn28.Location = new System.Drawing.Point(250, 64);
            this.btn28.Name = "btn28";
            this.btn28.Size = new System.Drawing.Size(32, 32);
            this.btn28.TabIndex = 26;
            this.btn28.UseVisualStyleBackColor = true;
            this.btn28.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ButtonClickCheck);
            // 
            // btn27
            // 
            this.btn27.Location = new System.Drawing.Point(219, 64);
            this.btn27.Name = "btn27";
            this.btn27.Size = new System.Drawing.Size(32, 32);
            this.btn27.TabIndex = 25;
            this.btn27.UseVisualStyleBackColor = true;
            this.btn27.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ButtonClickCheck);
            // 
            // btn26
            // 
            this.btn26.Location = new System.Drawing.Point(188, 64);
            this.btn26.Name = "btn26";
            this.btn26.Size = new System.Drawing.Size(32, 32);
            this.btn26.TabIndex = 24;
            this.btn26.UseVisualStyleBackColor = true;
            this.btn26.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ButtonClickCheck);
            // 
            // btn25
            // 
            this.btn25.Location = new System.Drawing.Point(157, 64);
            this.btn25.Name = "btn25";
            this.btn25.Size = new System.Drawing.Size(32, 32);
            this.btn25.TabIndex = 23;
            this.btn25.UseVisualStyleBackColor = true;
            this.btn25.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ButtonClickCheck);
            // 
            // btn24
            // 
            this.btn24.Location = new System.Drawing.Point(126, 64);
            this.btn24.Name = "btn24";
            this.btn24.Size = new System.Drawing.Size(32, 32);
            this.btn24.TabIndex = 22;
            this.btn24.UseVisualStyleBackColor = true;
            this.btn24.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ButtonClickCheck);
            // 
            // btn23
            // 
            this.btn23.Location = new System.Drawing.Point(95, 64);
            this.btn23.Name = "btn23";
            this.btn23.Size = new System.Drawing.Size(32, 32);
            this.btn23.TabIndex = 21;
            this.btn23.UseVisualStyleBackColor = true;
            this.btn23.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ButtonClickCheck);
            // 
            // btn22
            // 
            this.btn22.Location = new System.Drawing.Point(64, 64);
            this.btn22.Name = "btn22";
            this.btn22.Size = new System.Drawing.Size(32, 32);
            this.btn22.TabIndex = 20;
            this.btn22.UseVisualStyleBackColor = true;
            this.btn22.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ButtonClickCheck);
            // 
            // btn21
            // 
            this.btn21.Location = new System.Drawing.Point(33, 64);
            this.btn21.Name = "btn21";
            this.btn21.Size = new System.Drawing.Size(32, 32);
            this.btn21.TabIndex = 19;
            this.btn21.UseVisualStyleBackColor = true;
            this.btn21.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ButtonClickCheck);
            // 
            // btn20
            // 
            this.btn20.Location = new System.Drawing.Point(2, 64);
            this.btn20.Name = "btn20";
            this.btn20.Size = new System.Drawing.Size(32, 32);
            this.btn20.TabIndex = 18;
            this.btn20.UseVisualStyleBackColor = true;
            this.btn20.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ButtonClickCheck);
            // 
            // btn18
            // 
            this.btn18.Location = new System.Drawing.Point(250, 33);
            this.btn18.Name = "btn18";
            this.btn18.Size = new System.Drawing.Size(32, 32);
            this.btn18.TabIndex = 17;
            this.btn18.UseVisualStyleBackColor = true;
            this.btn18.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ButtonClickCheck);
            // 
            // btn17
            // 
            this.btn17.Location = new System.Drawing.Point(219, 33);
            this.btn17.Name = "btn17";
            this.btn17.Size = new System.Drawing.Size(32, 32);
            this.btn17.TabIndex = 16;
            this.btn17.UseVisualStyleBackColor = true;
            this.btn17.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ButtonClickCheck);
            // 
            // btn16
            // 
            this.btn16.Location = new System.Drawing.Point(188, 33);
            this.btn16.Name = "btn16";
            this.btn16.Size = new System.Drawing.Size(32, 32);
            this.btn16.TabIndex = 15;
            this.btn16.UseVisualStyleBackColor = true;
            this.btn16.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ButtonClickCheck);
            // 
            // btn15
            // 
            this.btn15.Location = new System.Drawing.Point(157, 33);
            this.btn15.Name = "btn15";
            this.btn15.Size = new System.Drawing.Size(32, 32);
            this.btn15.TabIndex = 14;
            this.btn15.UseVisualStyleBackColor = true;
            this.btn15.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ButtonClickCheck);
            // 
            // btn14
            // 
            this.btn14.Location = new System.Drawing.Point(126, 33);
            this.btn14.Name = "btn14";
            this.btn14.Size = new System.Drawing.Size(32, 32);
            this.btn14.TabIndex = 13;
            this.btn14.UseVisualStyleBackColor = true;
            this.btn14.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ButtonClickCheck);
            // 
            // btn13
            // 
            this.btn13.Location = new System.Drawing.Point(95, 33);
            this.btn13.Name = "btn13";
            this.btn13.Size = new System.Drawing.Size(32, 32);
            this.btn13.TabIndex = 12;
            this.btn13.UseVisualStyleBackColor = true;
            this.btn13.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ButtonClickCheck);
            // 
            // btn12
            // 
            this.btn12.Location = new System.Drawing.Point(64, 33);
            this.btn12.Name = "btn12";
            this.btn12.Size = new System.Drawing.Size(32, 32);
            this.btn12.TabIndex = 11;
            this.btn12.UseVisualStyleBackColor = true;
            this.btn12.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ButtonClickCheck);
            // 
            // btn11
            // 
            this.btn11.Location = new System.Drawing.Point(33, 33);
            this.btn11.Name = "btn11";
            this.btn11.Size = new System.Drawing.Size(32, 32);
            this.btn11.TabIndex = 10;
            this.btn11.UseVisualStyleBackColor = true;
            this.btn11.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ButtonClickCheck);
            // 
            // btn10
            // 
            this.btn10.Location = new System.Drawing.Point(2, 33);
            this.btn10.Name = "btn10";
            this.btn10.Size = new System.Drawing.Size(32, 32);
            this.btn10.TabIndex = 9;
            this.btn10.UseVisualStyleBackColor = true;
            this.btn10.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ButtonClickCheck);
            // 
            // btn08
            // 
            this.btn08.Location = new System.Drawing.Point(250, 2);
            this.btn08.Name = "btn08";
            this.btn08.Size = new System.Drawing.Size(32, 32);
            this.btn08.TabIndex = 8;
            this.btn08.UseVisualStyleBackColor = true;
            this.btn08.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ButtonClickCheck);
            // 
            // btn07
            // 
            this.btn07.Location = new System.Drawing.Point(219, 2);
            this.btn07.Name = "btn07";
            this.btn07.Size = new System.Drawing.Size(32, 32);
            this.btn07.TabIndex = 7;
            this.btn07.UseVisualStyleBackColor = true;
            this.btn07.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ButtonClickCheck);
            // 
            // btn06
            // 
            this.btn06.Location = new System.Drawing.Point(188, 2);
            this.btn06.Name = "btn06";
            this.btn06.Size = new System.Drawing.Size(32, 32);
            this.btn06.TabIndex = 6;
            this.btn06.UseVisualStyleBackColor = true;
            this.btn06.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ButtonClickCheck);
            // 
            // btn05
            // 
            this.btn05.Location = new System.Drawing.Point(157, 2);
            this.btn05.Name = "btn05";
            this.btn05.Size = new System.Drawing.Size(32, 32);
            this.btn05.TabIndex = 5;
            this.btn05.UseVisualStyleBackColor = true;
            this.btn05.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ButtonClickCheck);
            // 
            // btn04
            // 
            this.btn04.Location = new System.Drawing.Point(126, 2);
            this.btn04.Name = "btn04";
            this.btn04.Size = new System.Drawing.Size(32, 32);
            this.btn04.TabIndex = 4;
            this.btn04.UseVisualStyleBackColor = true;
            this.btn04.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ButtonClickCheck);
            // 
            // btn03
            // 
            this.btn03.Location = new System.Drawing.Point(95, 2);
            this.btn03.Name = "btn03";
            this.btn03.Size = new System.Drawing.Size(32, 32);
            this.btn03.TabIndex = 3;
            this.btn03.UseVisualStyleBackColor = true;
            this.btn03.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ButtonClickCheck);
            // 
            // btn02
            // 
            this.btn02.Location = new System.Drawing.Point(64, 2);
            this.btn02.Name = "btn02";
            this.btn02.Size = new System.Drawing.Size(32, 32);
            this.btn02.TabIndex = 2;
            this.btn02.UseVisualStyleBackColor = true;
            this.btn02.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ButtonClickCheck);
            // 
            // btn01
            // 
            this.btn01.Location = new System.Drawing.Point(33, 2);
            this.btn01.Name = "btn01";
            this.btn01.Size = new System.Drawing.Size(32, 32);
            this.btn01.TabIndex = 1;
            this.btn01.UseVisualStyleBackColor = true;
            this.btn01.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ButtonClickCheck);
            // 
            // btn00
            // 
            this.btn00.Location = new System.Drawing.Point(2, 2);
            this.btn00.Name = "btn00";
            this.btn00.Size = new System.Drawing.Size(32, 32);
            this.btn00.TabIndex = 0;
            this.btn00.UseVisualStyleBackColor = true;
            this.btn00.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ButtonClickCheck);
            // 
            // frmGameWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(309, 337);
            this.Controls.Add(this.pnlBoard);
            this.Controls.Add(this.lblTitle);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "frmGameWindow";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = ":: MineSweeper FTW! 2.0 ::";
            this.pnlBoard.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblTitle;
        private System.Windows.Forms.Panel pnlBoard;
        private System.Windows.Forms.Button btn88;
        private System.Windows.Forms.Button btn87;
        private System.Windows.Forms.Button btn86;
        private System.Windows.Forms.Button btn85;
        private System.Windows.Forms.Button btn84;
        private System.Windows.Forms.Button btn83;
        private System.Windows.Forms.Button btn82;
        private System.Windows.Forms.Button btn81;
        private System.Windows.Forms.Button btn80;
        private System.Windows.Forms.Button btn78;
        private System.Windows.Forms.Button btn77;
        private System.Windows.Forms.Button btn76;
        private System.Windows.Forms.Button btn75;
        private System.Windows.Forms.Button btn74;
        private System.Windows.Forms.Button btn73;
        private System.Windows.Forms.Button btn72;
        private System.Windows.Forms.Button btn71;
        private System.Windows.Forms.Button btn70;
        private System.Windows.Forms.Button btn68;
        private System.Windows.Forms.Button btn67;
        private System.Windows.Forms.Button btn66;
        private System.Windows.Forms.Button btn65;
        private System.Windows.Forms.Button btn64;
        private System.Windows.Forms.Button btn63;
        private System.Windows.Forms.Button btn62;
        private System.Windows.Forms.Button btn61;
        private System.Windows.Forms.Button btn60;
        private System.Windows.Forms.Button btn58;
        private System.Windows.Forms.Button btn57;
        private System.Windows.Forms.Button btn56;
        private System.Windows.Forms.Button btn55;
        private System.Windows.Forms.Button btn54;
        private System.Windows.Forms.Button btn53;
        private System.Windows.Forms.Button btn52;
        private System.Windows.Forms.Button btn51;
        private System.Windows.Forms.Button btn50;
        private System.Windows.Forms.Button btn48;
        private System.Windows.Forms.Button btn47;
        private System.Windows.Forms.Button btn46;
        private System.Windows.Forms.Button btn45;
        private System.Windows.Forms.Button btn44;
        private System.Windows.Forms.Button btn43;
        private System.Windows.Forms.Button btn42;
        private System.Windows.Forms.Button btn41;
        private System.Windows.Forms.Button btn40;
        private System.Windows.Forms.Button btn38;
        private System.Windows.Forms.Button btn37;
        private System.Windows.Forms.Button btn36;
        private System.Windows.Forms.Button btn35;
        private System.Windows.Forms.Button btn34;
        private System.Windows.Forms.Button btn33;
        private System.Windows.Forms.Button btn32;
        private System.Windows.Forms.Button btn31;
        private System.Windows.Forms.Button btn30;
        private System.Windows.Forms.Button btn28;
        private System.Windows.Forms.Button btn27;
        private System.Windows.Forms.Button btn26;
        private System.Windows.Forms.Button btn25;
        private System.Windows.Forms.Button btn24;
        private System.Windows.Forms.Button btn23;
        private System.Windows.Forms.Button btn22;
        private System.Windows.Forms.Button btn21;
        private System.Windows.Forms.Button btn20;
        private System.Windows.Forms.Button btn18;
        private System.Windows.Forms.Button btn17;
        private System.Windows.Forms.Button btn16;
        private System.Windows.Forms.Button btn15;
        private System.Windows.Forms.Button btn14;
        private System.Windows.Forms.Button btn13;
        private System.Windows.Forms.Button btn12;
        private System.Windows.Forms.Button btn11;
        private System.Windows.Forms.Button btn10;
        private System.Windows.Forms.Button btn08;
        private System.Windows.Forms.Button btn07;
        private System.Windows.Forms.Button btn06;
        private System.Windows.Forms.Button btn05;
        private System.Windows.Forms.Button btn04;
        private System.Windows.Forms.Button btn03;
        private System.Windows.Forms.Button btn02;
        private System.Windows.Forms.Button btn01;
        private System.Windows.Forms.Button btn00;
    }
}

