﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MineSweeper2._0
{
    public partial class frmGameWindow : Form
    {
        private Button[,] btnGrid = new Button[9, 9];
        const int gridWidth = 9;
        const int gridHeight = 9;
        int bomb = 10, win = 0, flags = 0;
        bool gameOn = true;
        string sBomb = "B", flag = "X", question = "?";
        Queue<Button> buttonQueue = new Queue<Button>();

        public frmGameWindow()
        {
            InitializeComponent();
            FillGrid(btnGrid, pnlBoard);
            CreateBombs(bomb, btnGrid);
            SetFieldIndicators(gridWidth, gridHeight, btnGrid, sBomb);
        }

        /// <summary>
        /// Fill the btnGrid Array with created buttons.
        /// </summary>
        private void FillGrid(Button[,] btnGrid, Panel panel)
        {
            for (int i = 0; i < 9; i++)
            {
                for (int j = 0; j < 9; j++)
                {
                   string buttonName = "btn" + i.ToString() + j.ToString();
                   btnGrid[i, j] = FindButton(buttonName, panel);
                }
            }
        }

        /// <summary>
        /// Find and return a Button with the received name in the received Panel.
        /// </summary>
        /// <param name="buttonName">Buton Name Property</param>
        /// <param name="panel">Panel</param>
        /// <returns>Button</returns>
        private Button FindButton(string buttonName, Panel panel)
        {
            Button tempButton = new Button();
            foreach (Button button in panel.Controls)
            {
                if (buttonName.Equals(button.Name))
                {
                    tempButton = button;
                    tempButton.Tag = "";
                    break;
                }
            }

            return tempButton;
        }

        /// <summary>
        /// Create the received amount of bombs ramdomly in the receiver Button Array.
        /// </summary>
        /// <param name="bombAmount">Amount of Bombs</param>
        /// <param name="btnGrid">Button Array</param>
        private void CreateBombs(int bombAmount, Button[,] btnGrid)
        {
            Random rand = new Random();
            int x, y;
            bool theBombHasBeenPlanted = false;

            for (int i = 0; i < bombAmount; i++)
            {
                theBombHasBeenPlanted = false;

                do
                {
                    x = rand.Next(9);
                    y = rand.Next(9);
                    string btnName = "btn" + x.ToString() + y.ToString();

                    if (btnName.Equals(btnGrid[x, y].Name))
                    {
                        if (!btnGrid[x, y].Tag.Equals("B"))
                        {
                            btnGrid[x, y].Tag = "B";
                            theBombHasBeenPlanted = true;
                        }
                    }

                } while (!theBombHasBeenPlanted);
            }
            flags = bombAmount;
        }

        /// <summary>
        /// Receives the Button Array and it's bounds to loop for and set the field indicator values.
        /// </summary>
        /// <param name="width">Width of the Array</param>
        /// <param name="height">Height of the Array</param>
        /// <param name="btnGrid">Button Array</param>
        private void SetFieldIndicators(int width, int height, Button[,] btnGrid, string bomb)
        {
            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < height; j++)
                {
                    if (bomb.Equals(btnGrid[i, j].Tag.ToString()))
                    {
                        SetSurrounds(i, j, btnGrid, bomb);
                    }
                }
            }
        }

        /// <summary>
        /// Set the field indicator values surrounding a bomb.
        /// </summary>
        /// <param name="i">Array Line value</param>
        /// <param name="j">Array Collumn value</param>
        /// <param name="btnGrid">Button Array</param>
        /// <param name="bomb">bomb identificator</param>
        private void SetSurrounds(int i, int j, Button[,] btnGrid, string bomb)
        {
            int iTemp = (i - 1);
            int jTemp = (j - 1);

            for (int x = 0; x < 3; x++)
            {
                for (int y = 0; y < 3; y++)
                {
                    if (((iTemp >= 0) && (iTemp < gridWidth)) && ((jTemp >= 0) && (jTemp < gridHeight)))
                    {
                        if (!bomb.Equals(btnGrid[iTemp, jTemp].Tag.ToString()))
                        {
                            if (btnGrid[iTemp, jTemp].Tag.Equals(""))
                            {
                                btnGrid[iTemp, jTemp].Tag = "1";
                            }
                            else
                            {
                                btnGrid[iTemp, jTemp].Tag = ((Int32.Parse((string)btnGrid[iTemp, jTemp].Tag) + 1)).ToString();
                            }
                        }
                    }
                    jTemp++;
                }
                jTemp = (j - 1);
                iTemp++;
            }
        }

        /// <summary>
        /// Checks the surrounds of the Button received and process them.
        /// </summary>
        /// <param name="button">Button</param>
        private void ClearEmptyChainedField(Button button)
        {
            int iTemp = ((Int32.Parse(button.Name.Substring(3, 1))) - 1);
            int jTemp = ((Int32.Parse(button.Name.Substring(4, 1))) - 1);
            int jStart = ((Int32.Parse(button.Name.Substring(4, 1))) - 1);

            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    if (((iTemp >= 0) && (iTemp < gridWidth)) && ((jTemp >= 0) && (jTemp < gridHeight)))
                    {
                        string buttonName = "btn" + iTemp.ToString() + jTemp.ToString();

                        if (!btnGrid[iTemp, jTemp].Text.Equals(flag))
                        {
                            if (btnGrid[iTemp, jTemp].Tag.Equals(""))
                            {
                                if (btnGrid[iTemp, jTemp].Enabled)
                                {
                                    if (!IsButtonEnqueued(btnGrid[iTemp, jTemp]))
                                    {
                                        buttonQueue.Enqueue(btnGrid[iTemp, jTemp]);
                                    }
                                }
                            }
                            else
                            {
                                if (btnGrid[iTemp, jTemp].Enabled)
                                {
                                    btnGrid[iTemp, jTemp].Enabled = false;
                                    btnGrid[iTemp, jTemp].Text = btnGrid[iTemp, jTemp].Tag.ToString();
                                }
                            }
                        }
                    }
                    jTemp++;
                }
                jTemp = jStart;
                iTemp++;
            }

            if (buttonQueue.Count > 0)
            {
                Button tempButton = (Button)buttonQueue.Dequeue();

                if (tempButton.Enabled)
                {
                    tempButton.Enabled = false;

                    if (tempButton.Text.Equals(question))
                    {
                        tempButton.Text = "";
                    }
                }
                ClearEmptyChainedField(tempButton);
            }
        }

        /// <summary>
        /// Checks if the received Button is already enqueued.
        /// </summary>
        /// <param name="button">Button</param>
        /// <returns>Boolean</returns>
        private bool IsButtonEnqueued(Button button)
        {
            bool result = false;

            if (buttonQueue.Count > 0)
            {
                if (buttonQueue.Contains(button))
                {
                    result = true;
                }
            }

            return result;
        }

        /// <summary>
        /// Resets the game.
        /// </summary>
        /// <param name="btnGrid">Array to Reset</param>
        private void ResetGame(Button[,] btnGrid)
        {
            for (int i = 0; i < gridWidth; i++)
            {
                for (int j = 0; j < gridHeight; j++)
                {
                    btnGrid[i, j].Tag = "";
                    btnGrid[i, j].Text = "";
                    btnGrid[i, j].Enabled = true;
                    btnGrid[i, j].BackColor = default(Color);
                    btnGrid[i, j].UseVisualStyleBackColor = true;
                }
            }
            CreateBombs(bomb, btnGrid);
            SetFieldIndicators(gridWidth, gridHeight, btnGrid, sBomb);
            gameOn = true;
            win = 0;
        }

        /// <summary>
        /// Checks which Mouse Button has been clicked.
        /// </summary>
        /// <param name="sender">The current Button that has been clicked.</param>
        /// <param name="e">Events from the Mouse.</param>
        private void ButtonClickCheck(object sender, MouseEventArgs e)
        {
            Button button = (Button)sender;

            if (e.Button == MouseButtons.Right)
            {
                if (button.Text.Equals(flag))
                {
                    button.Text = question;
                    flags++;
                }
                else if (button.Text.Equals(question))
                {
                    button.Text = "";
                }
                else
                {
                    if (flags > 0)
                    {
                        button.Text = flag;
                        flags--;
                    }
                }
                return;
            }

            ButtonClick(button, e);
        }

        /// <summary>
        /// Event that happens when a button is clicked. The game logic runs in here.
        /// </summary>
        /// <param name="sender">Button pressed</param>
        /// <param name="e">Details about the Button</param>
        private void ButtonClick(object sender, EventArgs e)
        {
            Button button = (Button)sender;

            if (gameOn)
            {
                if (!button.Text.Equals(flag))
                {
                    button.Enabled = false;

                    if (button.Tag.Equals(sBomb))
                    {
                        for (int i = 0; i < gridWidth; i++)
                        {
                            for (int j = 0; j < gridHeight; j++)
                            {
                                if (btnGrid[i, j].Tag.Equals(sBomb))
                                {
                                    btnGrid[i, j].Text = sBomb;
                                    btnGrid[i, j].BackColor = Color.Red;
                                    gameOn = false;
                                }
                            }
                        }

                        MessageBox.Show("BOOM! You blew yourself up!", "GameOver!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        DialogResult diagResult = MessageBox.Show("Do You Wanna Play Again?", "Play Again?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                        if (diagResult == DialogResult.Yes)
                        {
                            ResetGame(btnGrid);
                        }
                        else
                        {
                            this.Close();
                        }
                    }
                    else if (button.Tag.Equals(""))
                    {
                        if (button.Text.Equals(question))
                        {
                            button.Text = "";
                        }
                        ClearEmptyChainedField(button);
                    }
                    else
                    {
                        button.Text = button.Tag.ToString();
                    }
                }

                for (int i = 0; i < gridWidth; i++)
                {
                    for (int j = 0; j < gridHeight; j++)
                    {
                        if (btnGrid[i, j].Enabled)
                        {
                            win++;
                        }
                    }
                }

                if (win == bomb)
                {
                    MessageBox.Show("NICE! You survived the Minefield!", "Winner!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    DialogResult diagResult = MessageBox.Show("Do You Wanna Play Again?", "Play Again?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                    if (diagResult == DialogResult.Yes)
                    {
                        ResetGame(btnGrid);
                    }
                    else
                    {
                        this.Close();
                    }
                }
                else
                {
                    win = 0;
                }
            }
        }

    }
}
